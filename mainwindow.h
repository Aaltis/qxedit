#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QtGui>
#include <QtWidgets>
#include <QFileInfo>
#include <QList>
#include <QXmlStreamWriter>
#include "schematreewidget.h"
#include "schemamodel.h"
#include "xmltableviewwidget.h"
#include "xmltableitem.h"

class QAction;
class QActionGroup;
class QMenu;

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow();

protected:
    void contextMenuEvent(QContextMenuEvent *event);

private:
    void createActions();
    void createMenus();


    QList<schemaModel *> treeItems;
    SchemaTreeWidget *schemaWidget;
    XmlTableViewWidget *xmlTable;
    QList<XmlTableItem *> xmlTableItems;
    QStringList horzHeaders;
    QMenu *fileMenu;
    QMenu *editMenu;
    QMenu *formatMenu;
    QMenu *helpMenu;
    QLineEdit *schemaName;
    QLineEdit *schemaType;
    QAction *newAct;
    QAction *openAct;
    QAction *saveAct;
    QAction *exitAct;
    QAction *undoAct;
    QAction *redoAct;
    QAction *cutAct;
    QAction *copyAct;
    QAction *pasteAct;
    QAction *aboutAct;
    QAction *aboutQtAct;
    QAction *newSchemaItemAct;
    QAction *newRowAct;

signals:

private slots:
    void newRow();
    void newFile();
    void open();
    void save();
    void undo();
    void redo();
    void cut();
    void copy();
    void paste();
    void about();
    void aboutQt();
    void newSchemaItem();

public slots:


};

#endif // MAINWINDOW_H
