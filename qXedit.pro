# Add more folders to ship with the application, here
folder_01.source = qml/qXedit
folder_01.target = qml
DEPLOYMENTFOLDERS = folder_01

# Additional import path used to resolve QML modules in Creator's code model
QML_IMPORT_PATH =

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

# The .cpp file which was generated for your project. Feel free to hack it.
SOURCES += main.cpp \
    mainwindow.cpp \
    schematreewidget.cpp \
    xmltableviewwidget.cpp \
    schemamodel.cpp \
    xmltableitem.cpp

# Installation path
# target.path =

# Please do not modify the following two lines. Required for deployment.
include(qtquick2applicationviewer/qtquick2applicationviewer.pri)
qtcAddDeployment()

HEADERS += \
    mainwindow.h \
    schematreewidget.h \
    xmltableviewwidget.h \
    schemamodel.h \
    xmltableitem.h
